import random
import time
from turtle import Screen
from player import Player
from car_manager import CarManager
from scoreboard import Scoreboard

# initialize screen
screen = Screen()
screen.setup(width=600, height=600)
screen.tracer(0)

# instantiate player
player = Player()

# instantiate car_manager
car_manager = CarManager()

# enable listener and player moving
screen.listen()
screen.onkey(player.move, "Up")

# instantiate scoreboard
scoreboard = Scoreboard()

# game logic
game_is_on = True
while game_is_on:
    time.sleep(0.1)
    screen.update()

    # 25% chance to generate a car -> car generated every 0.4 seconds on average
    if random.randint(0, 3) == 3:
        car = car_manager.generate_car()
        car_manager.cars.append(car)

    # move all cars
    car_manager.move_all_cars()

    # car and turtle collision detection
    for car in car_manager.cars:
        if player.distance(car) < 30:
            game_is_on = False
            scoreboard.game_over()

    # player passes a level and next one is initiated
    if player.ycor() > 280:
        player.reset_position()
        scoreboard.level_up()
        car_manager.increase_speed()

screen.exitonclick()
